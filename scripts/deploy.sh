#!/usr/bin/env bash

function check_okay() {
  if [ $? -ne '0' ]
  then
    exit 1;
  fi
}

SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
DOCROOT=${DOCROOT:-$SCRIPTDIR/../web};
SITEROOT=$SCRIPTDIR/../;
echo "Deploying in $DOCROOT...";
cd $DOCROOT;
check_okay;
echo "Pulling from repo...";
git pull --rebase;
check_okay;
cd $SITEROOT;
echo "Updating composer dependencies...";
composer install;
check_okay;
cd $DOCROOT;
echo "Updating database...";
drush updb -y;
check_okay;
echo "Syncing config...";
drush cim sync -y;
check_okay;
echo "Rebuilding caches...";
drush cr all
check_okay;
